
fn main() {
    let limit = 1000;
    let mut a = 1;
    let mut b = 2;
    let mut c = 3;

    while a < limit {
        while b < limit {
            while c < limit {
                let sum = a + b + c;
                let order = in_order(a, b, c);
                let is_pyth = pyth(a, b, c);
                if order && is_pyth {
                    println!("({}, {}, {})={} -> (p={}, o={})",
                             a,
                             b,
                             c,
                             sum,
                             is_pyth,
                             order);

                    // stop when answer is found
                    if sum == limit {
                        println!("\nAnswer:\n\t {} * {} * {} = {}", a, b, c, a * b * c);
                        return;
                    }
                }

                c += 1;
                if c == limit {
                    c = 1;
                    break;
                }
            }
            b += 1;
            if b == limit {
                b = 1;
                break;
            }
        }
        a += 1;
    }

}


fn in_order(a: i32, b: i32, c: i32) -> bool {
    if a >= b {
        return false;
    }
    if b >= c {
        return false;
    }
    true
}

fn pyth(a: i32, b: i32, c: i32) -> bool {
    let sum: i32 = a * a + b * b;
    let square: i32 = c * c;
    if sum == square {
        return true;
    }
    false
}
