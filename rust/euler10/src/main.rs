extern crate num;

use num::Num;

fn main() {
    let mut sum: i64 = 0;
    for x in 0..2000000 {
        if is_prime(x) && x != 1 {
            sum += x;
            println!("is_prime: {} = {}, sum: {}", x, is_prime(x), sum);
        }
    }
}

fn is_prime<N: Num + PartialOrd + Copy>(n: N) -> bool {
    let _0 = N::zero();
    let _1 = N::one();
    let _2 = _1 + _1;
    let _3 = _2 + _1;
    let _5 = _2 + _3;
    let _6 = _3 + _3;
    if n == _2 || n == _3 {
        return true;
    } else if n % _2 == _0 || n % _3 == _0 {
        return false;
    }

    let mut i = _5;
    let mut w = _2;
    while i * i <= n {
        if n % i == _0 {
            return false;
        }
        i = i + w;
        w = _6 - w;
    }
    true
}
