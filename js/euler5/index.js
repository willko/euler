/**
 * returns true if num is divisible by all numbers from 1 to 20
 */
const isDivisible = num => (
  num % 11 === 0 &&
  num % 12 === 0 &&
  num % 13 === 0 &&
  num % 14 === 0 &&
  num % 15 === 0 &&
  num % 16 === 0 &&
  num % 17 === 0 &&
  num % 18 === 0 &&
  num % 19 === 0 &&
  num % 20 === 0
);

console.time('executionTime');
let found = null;
let i = 2520; // smallest number divisible by #s 1..to..10
while (!found) {
  if (isDivisible(++i)) {
    found = i;
  }
}

console.log(`Answer: ${found}`);
console.timeEnd('executionTime');

