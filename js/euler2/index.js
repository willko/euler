
const fibs = [ 1, 1, 2 ];

let last = 2;
let sum = 2;

while (last <= 4000000) {

  // get the last index
  const end = fibs.length - 1;

  // calculate the next fibinacci number
  last = fibs[end] + fibs[end - 1];

  // add to evens array if not odd
  if (last % 2 === 0) {
    sum += last;
  }

  fibs.push(last);

}

console.log('sum:', sum);

