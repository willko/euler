const isPalindrome = num => {
  const og = `${num}`;
  const rev = og.split('').reverse().join('');
  return og === rev;
};

const getMax3DigitPalindromeProduct = num => {
  for (let i = num; i > 99; i--) {
    const product = num * i;
    if (isPalindrome(product)) {
      return [ num, i, product ];
    }
  }
  return null;
};


console.time('executionTime');
let op1 = 0;
let op2 = 0;
let max = 0;

for (let i = 999; i > 99; i--) {
  const results = getMax3DigitPalindromeProduct(i);
  if (results && results[2] > max) {
    op1 = i;
    op2 = results[1];
    max = results[2];
  }
}
console.log(`answer: ${op1} * ${op2} = ${max}`);
console.timeEnd('executionTime');
