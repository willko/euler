
const goodNumbers = [];



const isDivisible = num => num % 3 === 0 || num % 5 === 0;




for (let i = 0; i < 1000; i++) {
  if (isDivisible(i)) {
    goodNumbers.push(i);
  }
}



console.log(`answer: ${goodNumbers.reduce((prev, next) => prev + next )}`);



