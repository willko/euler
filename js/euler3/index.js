
function findLargestDivisiblePrimeUnder(target)
{
  let start = 2;
  let checkedFactorCount = 0;

  for (let i = start; i < target; i++) {
    if (target % i === 0) {
      const highEndFactor = target / i;

      if (isPrime(highEndFactor)) {
        // found the answer
        process.stdout.write('found:      \n\r');
        return highEndFactor;
      }

      process.stdout.write(`checked: ${++checkedFactorCount}  \r`);
    }
  }
  return null;
}

function isPrime(num)
{
  for (let i = 2; i < num; i++) {
    const factor = num / i;
    if (factor % 1 === 0) {
      return false;
    }
  }
  return true;
}

console.time('executionTime');
console.log(`\n  answer: ${findLargestDivisiblePrimeUnder(600851475143)}  \n`);
console.timeEnd('executionTime');
