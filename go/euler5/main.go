package main

// brute force way

import "fmt"

func main() {
	// removed factors of 20 (step-size == 20)
	sl := []int{3, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19}
	m  := 20
	
	for {
		if allFactor(sl, m) {
			fmt.Printf("LCM Found: %d \n", m)
			break
		}
		m += 20
	}
}

func allFactor(list []int, n int) bool {
	for _, v := range list {
		if n % v != 0 {
			return false
		}
	}
	return true
}
