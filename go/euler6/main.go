package main


import "fmt"

func main() {
	num := 100
	diff := calcDiff(num)
	fmt.Printf("The diff for %d: %d\n", num, diff)
}

func calcDiff(n int) int {
	sumOfSquares := 0
	sum := 0
	for i := 1; i <= n; i++ {
		sumOfSquares += i * i
		sum += i
	}
	return (sum * sum - sumOfSquares)
}