package main

import "fmt"

func main() {
	sum := sumOfMults(1000)
	fmt.Println("SumOfMults of 1000 is: ", sum)
}

func sumOfMults(num int) int {
	result := 0
	for i := 0; i < num; i++ {
		if i%5 == 0 || i%3 == 0 {
			result += i
		}
	}
	return result
}
