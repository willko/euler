package main

import "fmt"

func main() {
	//decrement testers & result slice
	dec1 := 999
	dec2 := 999
	var max int
	resultSL := []int{}

	// loop until all 3 digit combos are exhausted
	for dec1 >= 100 && dec2 >= 100 {
		test := dec1 * dec2
		str := fmt.Sprintf("%d", test)
		if isPalin(str) {
			resultSL = append(resultSL, test)
			if max < test { max = test }
		}
		dec1 -= 1
		if dec1 == 99 {
			dec2 -= 1
			dec1 = dec2
		}
	}
	// display the max value to the console
	fmt.Printf("MAX palindrome found was: %d \n", max)
}

// returns true if string is a palindrome
func isPalin(str string) bool {
	for i := 0; i < len(str); i++ {
		if i >= (len(str) - (i + 1)) {
			break
		}
		if str[i] != str[len(str)-(i+1)] {
			return false
		}
	}
	return true
}
