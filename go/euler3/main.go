package main

import "fmt"

func main() {
	NUM := 600851475143
	facs := getFactors(NUM, getLargestDivisor(NUM))
	for i, v := range facs {
		if getLargestDivisor(v) == 1 {
			fmt.Printf("Primes[%d]: %d \n", i, v)
		}
	}
}

// returns the largest divisor of num
func getLargestDivisor(num int) int {
	for i := 2; i <= num; i++ {
		if num%i == 0 {
			return num / i
		}
	}
	return 1
}

// returns a slice of ints that are factors of
// num upto the given max value.
func getFactors(num, max int) []int {
	factors := []int{1}
	for i := 2; i <= max; i++ {
		if num%i == 0 {
			//stop searching once middle is passed
			if hasVal(num/i, factors) {
				break
			}
			factors = append(factors, i)
		}
	}
	//add larger half after search is complete
	for k := len(factors) - 1; k >= 0; k-- {
		factors = append(factors, (num / factors[k]))
	}
	return factors
}

// returns true if target is in sl array
func hasVal(target int, sl []int) bool {
	for i := range sl {
		if sl[i] == target {
			return true
		}
	}
	return false
}
