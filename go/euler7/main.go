package main

import "fmt"

func main() {
	num := 10001
	pr := getPrime(num)
	fmt.Printf("prime[%d] is: %d \n", num, pr)
}

func getPrime(index int) int {
	count := 0
	for i := 2; index > 1; i++ {
		if getLargestDivisor(i) == 1 {
			count += 1
			if count == index {
				return i
			}
		}
	}
	return 1
}

// returns the largest divisor of num
func getLargestDivisor(num int) int {
	for i := 2; i <= num; i++ {
		if num%i == 0 {
			return num / i
		}
	}
	return 1
}
