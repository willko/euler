package main

// uses just int instead of big.Int

import (
  "fmt"
)

func main() {
	MAX := 4000000
	sum := 0
	for i := 1; i < 90; i++ {
		fib := fibonacci(i);
		if fib > MAX { break }
		if fib % 2 == 0 {
			sum += fib
		}
	}
	fmt.Println("SumOfEvens: ", sum)
}


func fibonacci(h int) int {

	// ---------------------------
	// added pre-calculated values
	// for performance boost
	switch h {
	case 30:
		return 832040
	case 29:
		return 514229
		//normal fibonacci algorithm...
	case 1:
		return 1
	case 0:
		return 0
	default:
		return fibonacci(h-1) + fibonacci(h-2)
	}
}
