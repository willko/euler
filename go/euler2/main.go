package main

import (
  "fmt"
  "math/big"
)

func main() {
	MAX := big.NewInt(4000000)
	H := big.NewInt(2)
	sum := new(big.Int)
	mod := new(big.Int)

	for i := 1; i < 90; i++ {
		fib := fibonacci(i);
		if fib.Cmp(MAX) > 0 { break }
		mod.Mod(fib, H)
		if mod.Cmp(big.NewInt(0)) == 0 {
			sum.Add(sum, fib)
		}
	}
	fmt.Println("SumOfEvens: ", sum)
}


func fibonacci(h int) *big.Int {

	// ---------------------------
	// added pre-calculated values
	// for performance boost
	switch h {
	case 90:
		return big.NewInt(2880067194370816120)
	case 89:
		return big.NewInt(1779979416004714189)
	case 80:
		return big.NewInt(23416728348467685)
	case 79:
		return big.NewInt(14472334024676221)
	case 50:
		return big.NewInt(12586269025)
	case 49:
		return big.NewInt(7778742049)
	case 30:
		return big.NewInt(832040)
	case 29:
		return big.NewInt(514229)
		//normal fibonacci algorithm...
	case 1:
		return big.NewInt(1)
	case 0:
		return big.NewInt(0)
	default:
		r := new(big.Int)
		return r.Add(fibonacci(h-1), fibonacci(h-2))
	}
}
