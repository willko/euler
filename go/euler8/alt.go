package main

import (
  "fmt"
  "strconv"
  "strings"
 )

func main() {
	num := "7316717653133062491922511967442657474235534919493496983520312774506326239578318016984801869478851843858615607891129494954595017379583319528532088055111254069874715852386305071569329096329522744304355766896648950445244523161731856403098711121722383113622298934233803081353362766142828064444866452387493035890729629049156044077239071381051585930796086670172427121883998797908792274921901699720888093776657273330010533678812202354218097512545405947522435258490771167055601360483958644670632441572215539753697817977846174064955149290862569321978468622482839722413756570560574902614079729686524145351004748216637048440319989000889524345065854122758866688116427171479924442928230863465674813919123162824586178664583591245665294765456828489128831426076900422421902267105562632111110937054421750694165896040807198403850962455444362981230987879927244284909188845801561660979191338754992005240636899125607176060588611646710940507754100225698315520005593572972571636269561882670428252483600823257530420752963450"
  seqLen := 13
  res := getLgAdjPr(num, seqLen)
	fmt.Printf("\nAnswer is: %d \n", res)
}


func getLgAdjPr(bank string, digits int) int {
	
  // split by zeros into array of strings
	sL := strings.Split(bank, "0")

  // convert sL to integer arrays of size 13 or more
  newBank := [][]int{}
  for i := range sL {
  	tmp := []int{}
  	if len(sL[i]) >= 13 {
	  	for j := range sL[i] {
	  		v, err := strconv.Atoi(string(sL[i][j]))
	  	  if err != nil { return -1 }
	  	  tmp = append(tmp, v)
	  	}
	  	newBank = append(newBank, tmp)
    }
  }
  // handle case of zeros in every sequence
  if len(newBank) == 0 { return 0 }

  //run sum algorithm on newBank
  candidates := [][]int{}
	for _, v := range newBank {
			if len(v) < 13 { continue }
			if len(v) == 13 {
			  if len(candidates) == 0 {
			  	candidates = append(candidates, v)
			  	continue
			  }
				if sumWithSkips(v) >= sumWithSkips(candidates[0]) {
					candidates = append(candidates, v)
				}
			} else {
				// iter over all 13 length sets of array
				for i := range v {
					//grab upto 13 digits at a time from long strings
					seq := []int{}
					for j := i; j < (i+13) && ((len(v) - i) >= 13); j++ {
					  seq = append(seq, v[j])
				  }
				  if len(seq) < 13 { continue }
				  //empty check
				  if len(candidates) == 0 {
				  	candidates = append(candidates, seq)
				  	continue
				  }
					if sumWithSkips(seq) >= sumWithSkips(candidates[0]) {
						candidates = append(candidates, seq)
					}
				}
			}
	}

	fmt.Println("Reduced set length: ", len(candidates))
  fmt.Println("\nProduct Calculations:")

	//now step though candidates and determine max
	max := product(candidates[0])
	ms  := sumWithSkips(candidates[0])
	index := -1
	for i, c := range candidates {
		if i == 0 { continue }
		nextSum := sumWithSkips(c)
		if ms <= nextSum {
			ms = nextSum
			nextProd := product(c)
			fmt.Printf("%v => %d\n", c, nextProd)
			if max < nextProd {
				max = nextProd
				index = i
		  }
		}
	}
	fmt.Printf("\n\nmaxSum: %d, seq: %v\n", ms, candidates[index])
  return max
}


func sumWithSkips(b []int) int {
	sum := 0
  for _, v := range b  {
  	if v == 1 { continue }
  	sum += v
  }
  return sum
}



func product(sl []int) int {
	res := 1
	for _, v := range sl {
		res *= v
	}
	return res
}





